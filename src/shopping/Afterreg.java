package shopping;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

@WebServlet("/Afterreg")
public class Afterreg extends HttpServlet {


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				response.setContentType("text/html");
				PrintWriter out=response.getWriter();
				out.print("<h2>User Successfully Registered</h2>");
				String uname=request.getParameter("uname");
				String pwd=request.getParameter("pwd");
				String phnno=request.getParameter("phnno");
				String addr=request.getParameter("addr");
				String gender=request.getParameter("gender");
				String cat=request.getParameter("cat");
				String comment=request.getParameter("comment");
				out.print("Username :"+uname+"<br>");
				out.print("Password :"+pwd+"<br>");
				out.print("Phone number :"+phnno+"<br>");
				out.print("Address :"+addr+"<br>");
				out.print("Gender :"+gender+"<br>");
				out.print("Category :"+cat+"<br>");
				out.print("Comment :"+comment+"<br>");
				
				StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
		          
				Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  
	
				SessionFactory factory = meta.getSessionFactoryBuilder().build();  
				Session session = factory.openSession(); 
				Transaction t=null;
				try
				{
				t=session.beginTransaction();
				User newuser=new User(uname,pwd,phnno,addr,gender,cat,comment);
				session.save(newuser);
				t.commit();
				}
				catch(Exception e)
				{
					t.rollback();
					e.printStackTrace();
				}
				finally
				{
					session.close();
				}
				
				
				
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
