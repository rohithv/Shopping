package shopping;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		 HttpSession ses=request.getSession();  
	         
		String uname=request.getParameter("uname");
		 ses.setAttribute("uname",uname);
		String pwd=request.getParameter("pwd");
		String sql = "FROM User WHERE UNAME = :uname and PWD = :pwd";
		
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
        
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  
		 
		SessionFactory factory = meta.getSessionFactoryBuilder().build();  
		Session session = factory.openSession(); 
		Query query = session.createQuery(sql);
		query.setParameter("uname", uname);
		query.setParameter("pwd", pwd);
		
		
		List results = query.list();
		if(results.size()==0)
		{
			out.print("Invalid Login");
			return;
		}
		out.println("<h2>Succeffully logged in</h2><br><h2>Welcome "+uname+"</h2>"
				+ "<form action='disp'><select name='cate'><option value=\"Laptops\">Laptops</option>"
				+ "<option value='Phones'>Phones</option></select>"
				+ "<input type=\"submit\" value=\"Submit\"></form><br>"
				+ "<form action='Edit'><input type='submit' value='Edit'></form>");
		System.out.println(results.size());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
