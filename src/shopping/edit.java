package shopping;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/edit")
public class edit extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
				out.print("<form action=\"dbupdate\" method=\"post\">\r\n" + 
						"Phone Number:\r\n" + 
						"<input type=\"text\" name=\"phnno\"/><br>\r\n" + 
						"Address:\r\n" + 
						"<input type=\"text\" name=\"addr\"/><br>\r\n" + 
						"<input type=\"submit\" value=\"Submit\"></form>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
