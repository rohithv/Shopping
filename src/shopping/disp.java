package shopping;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

@WebServlet("/disp")
public class disp extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String cate=request.getParameter("cate");
		System.out.println(cate);
		if(cate.equals("Laptops"))
		{
			String sql = "FROM laptops";
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
	        
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  
			 
			SessionFactory factory = meta.getSessionFactoryBuilder().build();  
			Session session = factory.openSession(); 
			Query query = session.createQuery(sql);
			List results = query.list();
			out.println("<table><th>Id</th><th>Name</th><th>Price</th></table>");
			for (Iterator iterator = results.iterator(); iterator.hasNext();){
	            laptops lap = (laptops) iterator.next(); 
	            out.print(lap.getId()+"|"); 
	            out.print(lap.getName()+"|"); 
	            out.println(lap.getPrice()+"|<br>"); 
	         }
		}
		else if(cate.equals("Phones"))
		{
			String sql = "FROM phones";
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
	        
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  
			 
			SessionFactory factory = meta.getSessionFactoryBuilder().build();  
			Session session = factory.openSession(); 
			Query query = session.createQuery(sql);
			List results = query.list();
			out.println("<table><th>Id</th><th>Name</th><th>Price</th></table>");
			for (Iterator iterator = results.iterator(); iterator.hasNext();){
	            phones lap = (phones) iterator.next(); 
	            out.print(lap.getId()+"|"); 
	            out.print(lap.getName()+"|"); 
	            out.println(lap.getPrice()+"|<br>"); 
	         }
		}
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
