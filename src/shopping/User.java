package shopping;

public class User {
private String uname;
private String pwd;
public User(String uname, String pwd, String phnno, String addr, String gender, String cat, String comment) {
	super();
	this.uname = uname;
	this.pwd = pwd;
	this.phnno = phnno;
	this.addr = addr;
	this.gender = gender;
	this.cat = cat;
	this.comment = comment;
}
public User()
{
}
private String phnno;
private String addr;
private String gender;
private String cat;
private String comment;
public String getUname() {
	return uname;
}
public void setUname(String uname) {
	this.uname = uname;
}
public String getPwd() {
	return pwd;
}
public void setPwd(String pwd) {
	this.pwd = pwd;
}
public String getPhnno() {
	return phnno;
}
public void setPhnno(String phnno) {
	this.phnno = phnno;
}
public String getAddr() {
	return addr;
}
public void setAddr(String addr) {
	this.addr = addr;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getCat() {
	return cat;
}
public void setCat(String cat) {
	this.cat = cat;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}


}
